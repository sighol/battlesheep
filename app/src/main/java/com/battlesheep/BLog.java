package com.battlesheep;

import android.util.Log;

/**
 * Created by Tor Idland on 3/17/14.
 */
public class BLog {

    public static void v(Object obj, String msg, Object... args) {
        String output = String.format(msg, args);
        Log.v(obj.getClass().toString(), output);
    }
}

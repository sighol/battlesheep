package com.battlesheep.network;

import java.util.Map;

abstract public class NewMoveCommand extends NetworkCommand {
    protected NewMoveCommand(Map<String, String> options) {
        super("bombsheep", options);
    }
}

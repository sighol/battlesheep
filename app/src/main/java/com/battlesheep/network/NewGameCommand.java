package com.battlesheep.network;

import java.util.Map;

abstract public class NewGameCommand extends NetworkCommand {
    protected NewGameCommand(Map<String, String> options) {
        super("newgame", options);
    }
}

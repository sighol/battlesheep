package com.battlesheep.network;


import android.os.AsyncTask;
import android.util.Log;

import com.battlesheep.BLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

abstract public class NetworkCommand extends AsyncTask<String, Void, String> {

    private static final String baseUrl = "http://battlesheep.herokuapp.com/";
    private String url;
    private Map<String, String> options;

    protected NetworkCommand(String url, Map<String, String> options) {
        if (options == null) {
            throw new NullPointerException("options can't be null");
        }
        this.url = url;
        this.options = options;
    }

    private static void log(String s, Object... args) {
        String out = String.format(s, args);
        Log.v("RUT", out);
    }

    public String getUrlParams() {
        ArrayList<String> params = new ArrayList<String>();
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : options.entrySet()) {
            if (builder.length() != 0) {
                builder.append("&");
            }
            String param = String.format("%s=%s", entry.getKey(), entry.getValue());
            builder.append(param);
        }
        return builder.toString();
    }

    @Override
    protected String doInBackground(String... strings) {
        if (strings.length != 0) {
            throw new IllegalArgumentException("doInBackground takes no url argument");
        }
        String url = baseUrl + this.url + "?" + getUrlParams();
        return sendRequest(url);
    }

    private String sendRequest(String urlString) {
        URL url = null;
        try {
            url = new URL(urlString);
            log("sendRequest: url= %s", url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            return out.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("malformedUrlException: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("ioexception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String json) {
        super.onPostExecute(json);
        BLog.v(this, "returnstr: %s", json);
        try {
            JSONObject obj = new JSONObject(json);
            String error = obj.getString("error");
            onError(error);
            return;
        } catch (JSONException e) {
            // Obviously, this wasn't an error...
        }
        try {
            onFinished(new JSONArray(json));
        } catch (JSONException e) {
            e.printStackTrace(); // FIXME vi burde kanskje hatt noen error-handling her
        }
    }

    abstract public void onFinished(JSONArray json) throws JSONException;

    public void onError(String msg) {
        Log.e("NetworkCommand", msg, new RuntimeException(msg));
    }


}

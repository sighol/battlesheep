package com.battlesheep.network;

import java.util.Map;


abstract public class PlaceSheepCommand extends NetworkCommand {
    protected PlaceSheepCommand(Map<String, String> options) {
        super("placesheep", options);
    }
}

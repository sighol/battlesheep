package com.battlesheep.network;

import java.util.Map;

abstract public class LoginCommand extends NetworkCommand {
    protected LoginCommand(Map<String, String> options) {
        super("login", options);
    }


}

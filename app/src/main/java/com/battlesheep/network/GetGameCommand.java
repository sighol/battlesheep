package com.battlesheep.network;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;


public class GetGameCommand extends NetworkCommand {
    protected GetGameCommand(Map<String, String> options) {
        super("getgame", options);
    }

    @Override
    public void onFinished(JSONArray json) throws JSONException {

    }
}

package com.battlesheep.network;

import java.util.Map;

abstract public class ExistingGamesCommand extends NetworkCommand {
    protected ExistingGamesCommand(Map<String, String> options) {
        super("existinggames", options);
    }
}

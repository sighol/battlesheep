package com.battlesheep.activities.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.battlesheep.R;
import com.battlesheep.activities.ActivityExtras;
import com.battlesheep.network.NetworkCommand;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Vegard on 31.03.14.
 */
public class GameFinishedActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_finished);

        String result = getIntent().getExtras().getString("result");
        final int userid = getIntent().getExtras().getInt(ActivityExtras.userId);
        final int gameid = getIntent().getExtras().getInt(ActivityExtras.gameId);
        TextView resultText = (TextView) findViewById(R.id.resultText);
        resultText.setText(result);

        Map<String, String> args = new HashMap<String, String>();
        args.put(ActivityExtras.gameId, "" + gameid);
        NetworkCommand cmd = new NetworkCommand("finishgame", args) {
            @Override
            public void onFinished(JSONArray json) throws JSONException {
                // Do nothing
            }
        };
        cmd.execute();


        Button finishGame_button = (Button) findViewById(R.id.bfinished);
        // Dersom vi kun kommer tilbake til game screen når finish game trykkes: kontakt vegard.
        finishGame_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GameFinishedActivity.this, MainMenuActivity.class);
                intent.putExtra(ActivityExtras.userId, userid);
                startActivity(intent);
                finish();
            }
        });

    }
}

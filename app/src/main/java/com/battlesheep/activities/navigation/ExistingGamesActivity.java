package com.battlesheep.activities.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.battlesheep.BLog;
import com.battlesheep.R;
import com.battlesheep.activities.ActivityExtras;
import com.battlesheep.activities.GameActivity;
import com.battlesheep.network.ExistingGamesCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExistingGamesActivity extends Activity {

    private int userid;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.existing_games);
        Bundle extras = getIntent().getExtras();
        userid = extras.getInt(ActivityExtras.userId);
        Map<String, String> map = new HashMap<String, String>();
        map.put(ActivityExtras.userId, "" + userid);
        final ArrayList<String> existingOppNames = new ArrayList<String>();
        final ArrayList<Integer> existingOppIds = new ArrayList<Integer>();
        final ArrayList<Integer> existingGameIds = new ArrayList<Integer>();

        ExistingGamesCommand cmd = new ExistingGamesCommand(map) {
            @Override
            public void onFinished(JSONArray json) throws JSONException {
                BLog.v(this, "nofgames: %d", json.length());
                for (int i = 0; i < json.length(); i++) {
                    JSONObject obj = json.getJSONObject(i).getJSONObject("user2");
                    String username = obj.getString("username");
                    int oppid = obj.getInt("id");
                    int gameid = json.getJSONObject(i).getInt("pk");
                    existingOppNames.add(i, username);
                    existingOppIds.add(i, oppid);
                    existingGameIds.add(i, gameid);
                    BLog.v(this, "user: %s", existingOppNames.get(i));
                    BLog.v(this, "userid: %d", existingOppIds.get(i));

                    listoperations(existingOppNames, existingGameIds, existingGameIds);
                }
            }
        };
        cmd.execute();


        Button cancelNewGame_button = (Button) findViewById(R.id.bcancel_exi);
        cancelNewGame_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    private void listoperations(ArrayList<String> namelist, final ArrayList<Integer> gameidlist, final ArrayList<Integer> useridlist) {
        ListView myListView = (ListView) findViewById(R.id.existing_list);
        ArrayAdapter<String> adapter;

        adapter = new ArrayAdapter<String>(ExistingGamesActivity.this, android.R.layout.simple_list_item_1, namelist);
        myListView.setAdapter(adapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                BLog.v(this, "user: %s", item);
                BLog.v(this, "index: %d", position);
                int gameid = gameidlist.get(position);
                Intent intent = new Intent(ExistingGamesActivity.this, GameActivity.class);
                intent.putExtra(ActivityExtras.gameId, gameid);
                intent.putExtra(ActivityExtras.userId, userid);
                startActivity(intent);
            }

        });

    }
}

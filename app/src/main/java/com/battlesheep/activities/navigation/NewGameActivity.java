package com.battlesheep.activities.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.battlesheep.BLog;
import com.battlesheep.R;
import com.battlesheep.activities.ActivityExtras;
import com.battlesheep.activities.GameActivity;
import com.battlesheep.network.NewGameCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vegard on 17.03.14.
 */
public class NewGameActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_game);

        Bundle extras = getIntent().getExtras();
        final int userid = extras.getInt(ActivityExtras.userId);
        BLog.v(this, "uid: %d", userid);

        Button startGame_button = (Button) findViewById(R.id.bstart_game);
        startGame_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText opponent = (EditText) findViewById(R.id.opp_username);
                Map<String, String> map = new HashMap<String, String>();
                map.put(ActivityExtras.userId, "" + userid);
                map.put("opponent", opponent.getText().toString());

                //Starter spillet med ønsket bruker
                NewGameCommand cmd = new NewGameCommand(map) {

                    @Override
                    public void onFinished(JSONArray json) throws JSONException {
                        JSONObject obj = json.getJSONObject(0);
                        JSONObject fields = obj.getJSONObject("fields");

                        BLog.v(this, "json: %s", obj.toString());

                        int gameid = obj.getInt("pk");
                        int userid = fields.getInt("user1");

                        Intent intent = new Intent(NewGameActivity.this, GameActivity.class);

                        intent.putExtra(ActivityExtras.gameId, gameid);
                        intent.putExtra(ActivityExtras.userId, userid);
                        startActivity(intent);
                    }

                    @Override
                    public void onError(String msg) {
                        Toast.makeText(NewGameActivity.this, msg, Toast.LENGTH_LONG);
                    }
                };
                cmd.execute();
            }
        });

        Button cancelNewGame_button = (Button) findViewById(R.id.bcancel_new);
        cancelNewGame_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}

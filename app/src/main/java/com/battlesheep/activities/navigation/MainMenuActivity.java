package com.battlesheep.activities.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.battlesheep.BLog;
import com.battlesheep.R;
import com.battlesheep.activities.ActivityExtras;

public class MainMenuActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        Bundle extras = getIntent().getExtras();
        final int userid = extras.getInt(ActivityExtras.userId);
        BLog.v(this, "uid: %d", userid);

        Button newGame_button = (Button) findViewById(R.id.bnew_game);
        newGame_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), NewGameActivity.class);
                myIntent.putExtra(ActivityExtras.userId, userid);
                startActivityForResult(myIntent, 0);
                BLog.v(this, "New GameModel");
            }
        });

        Button existingGames_button = (Button) findViewById(R.id.bexisting_game);
        existingGames_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Intent myIntent = new Intent(MainMenuActivity.this, ExistingGamesActivity.class);
                myIntent.putExtra(ActivityExtras.userId, userid);

                startActivity(myIntent);

            }
        });

    }
}

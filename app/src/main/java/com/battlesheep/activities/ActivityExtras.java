package com.battlesheep.activities;

/**
 * Created by Tor Idland on 3/29/14.
 */
public class ActivityExtras {

    public static final String userId = "userid";
    public static final String result = "result";
    public static final String gameId = "gameid";
}

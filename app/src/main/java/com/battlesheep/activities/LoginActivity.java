package com.battlesheep.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.battlesheep.BLog;
import com.battlesheep.R;
import com.battlesheep.activities.navigation.MainMenuActivity;
import com.battlesheep.network.LoginCommand;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        testingRequests();
    }

    private void testingRequests() {
        BLog.v(this, "Sending request");

        Button button = (Button) findViewById(R.id.blog_in);
        final EditText username = (EditText) findViewById(R.id.username);
        button.setOnClickListener(new View.OnClickListener() {

            int counter = 0;

            @Override
            public void onClick(View v) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("username", username.getText().toString());
                final TextView message = (TextView) findViewById(R.id.message);
                message.setText("Loading... please wait");

                LoginCommand cmd = new LoginCommand(map) {

                    @Override
                    public void onFinished(JSONArray json) throws JSONException {
                        message.setText("");

                        JSONObject obj = json.getJSONObject(0);
                        BLog.v(this, "Is finished %s", obj);
                        BLog.v(this, "id = %d", obj.getInt("pk"));
                        JSONObject fields = obj.getJSONObject("fields");
                        BLog.v(this, "username = %s", fields.getString("username"));

                        Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
                        intent.putExtra(ActivityExtras.userId, obj.getInt("pk"));
                        startActivity(intent);
                    }
                };
                cmd.execute();

            }
        });
    }

}

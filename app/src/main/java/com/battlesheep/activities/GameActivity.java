package com.battlesheep.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.battlesheep.R;
import com.battlesheep.game.models.BoardModel;
import com.battlesheep.game.models.GameModel;
import com.battlesheep.game.view.GameBoardView;
import sheep.game.Game;
import android.view.ViewGroup.LayoutParams;
import android.util.DisplayMetrics;
import android.content.Context;
import android.view.WindowManager;

/**
 * Created by oda on 11.03.14.
 */
public class GameActivity extends Activity {

    private GameModel gameModel;

    private TextView text;
    private Button placeSheepButton;
    private Button refreshButton;
    private BoardModel model;
    private int gameId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameId = getIntent().getIntExtra(ActivityExtras.gameId, -1);
        final int userId = getIntent().getIntExtra(ActivityExtras.userId, -1);

        // view game
        final Game game = new Game(this, null);

        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();  // Project settings: API <13

        gameModel = new GameModel(gameId, userId);
        model = new BoardModel(gameModel);
        final GameBoardView view = new GameBoardView(model, display, this);
        gameModel.load();


        game.pushState(view);
        setContentView(R.layout.game);

        // making a game.xml to use button in Android
        RelativeLayout linearLayout = (RelativeLayout) findViewById(R.id.bGameView);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(width, width));
        linearLayout.addView(game);

        text = (TextView) findViewById(R.id.text_expl);
        placeSheepButton = (Button) findViewById(R.id.doneButton);

        placeSheepButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                model.sendSheepPositions();
                placeSheepButton.setVisibility(v.INVISIBLE);
                refreshButton.setVisibility(v.VISIBLE);
                setText("Please wait for opponent! Click on <Refresh> to refresh ;);)");
            }
        });


        refreshButton = (Button) findViewById(R.id.brefresh);
          refreshButton.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                model.refresh();
              }
          });
    }

    public void setMyMeatMeter(){
        ImageView myMeatMeter = (ImageView) findViewById(R.id.meatmeter);

        Display  display= ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();

        //myMeatMeter.setMaxWidth(myMeatMeter.getWidth()*(int)model.getCompletionRate());
        //myMeatMeter.getLayoutParams().width = myMeatMeter.getWidth()*(int)model.getCompletionRate();
        myMeatMeter.setVisibility(1);

        LayoutParams params = (LayoutParams) myMeatMeter.getLayoutParams();
        params.width = (int)((double)width*model.getCompletionRate());
        myMeatMeter.setLayoutParams(params);
    }

    public void setOpponentMeatMeter(){
        ImageView opponentMeatMeter = (ImageView) findViewById(R.id.meatmeter2);

        Display  display= ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();


        // opponentMeatMeter.setMaxWidth(opponentMeatMeter.getWidth()*(int)model.getOpponentCompletionRate());
        //opponentMeatMeter.getLayoutParams().width = opponentMeatMeter.getWidth()*(int)model.getOpponentCompletionRate()CompletionRate();
        opponentMeatMeter.setVisibility(1);

        LayoutParams params = (LayoutParams) opponentMeatMeter.getLayoutParams();
        params.width = (int)((double)width*model.getOpponentCompletionRate());
        opponentMeatMeter.setLayoutParams(params);
    }

    public void setText(String txt) {
        text.setText(txt);
    }

    public void setBombMode() {
        refreshButton.setVisibility(View.VISIBLE);
        placeSheepButton.setVisibility(View.INVISIBLE);
    }

    public void setSheepPlacingMode() {
        placeSheepButton.setVisibility(View.VISIBLE);
        refreshButton.setVisibility(View.INVISIBLE);
    }

    public int getGameId() {
        return gameId;
    }
}

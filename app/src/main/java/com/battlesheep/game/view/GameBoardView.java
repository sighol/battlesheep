package com.battlesheep.game.view;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.Display;
import android.view.MotionEvent;
import com.battlesheep.activities.ActivityExtras;
import com.battlesheep.activities.GameActivity;
import com.battlesheep.activities.navigation.GameFinishedActivity;
import com.battlesheep.game.models.BoardModel;
import com.battlesheep.game.models.GameTile;
import com.battlesheep.game.models.OnChangeListener;
import java.util.ArrayList;
import sheep.game.State;
import sheep.math.Vector2;

public class GameBoardView extends State implements OnChangeListener<BoardModel> {

    private final GameActivity activity;
    private float width;
    private BoardModel model;
    private int tileSize;

    private boolean doneInitializing = false;

    private ArrayList<ArrayList<GameTile>> tiles;

    // CONSTRUCTOR
    public GameBoardView(BoardModel model, Display display, GameActivity gameActivity) {
        this.model = model;
        this.model.addListener(this);
        this.activity = gameActivity;
        width = display.getWidth();
        initializeTiles();
        doneInitializing = true;
    }

    @Override
    public void onChange(BoardModel model) {
        updateGameTiles(model);
        activity.setText(model.getMode());
        if (model.isInBombMode()) {
            activity.setBombMode();
        } else if (model.isInSheepPlacingMode()) {
            activity.setSheepPlacingMode();
        }
        activity.setMyMeatMeter();
        activity.setOpponentMeatMeter();

        checkIfFinished(model);
    }

    private void checkIfFinished(BoardModel model) {
        if (model.hasLost() || model.hasWon()) {
            Intent intent = new Intent(activity, GameFinishedActivity.class);
            intent.putExtra(ActivityExtras.gameId, activity.getGameId());
            if (model.hasWon()) {
                intent.putExtra("result", "You Won");
            } else if (model.hasLost()) {
                intent.putExtra("result", "You Lost");
            }
            activity.startActivity(intent);
        }
    }

    private void updateGameTiles(BoardModel model) {
        for (int i = 0; i < model.getRows(); i++) {
            for (int j = 0; j < model.getCols(); j++) {
                GameTile tile = tiles.get(i).get(j);
                tile.setTileState(model.get(i,j));
            }
        }
    }

    public void initializeTiles() {
        calculateTileSize();
        Vector2 sizeVec = new Vector2(tileSize, tileSize);
        ArrayList<ArrayList<GameTile>> tiles = new ArrayList<ArrayList<GameTile>>();
        for (int i = 0; i < this.model.getRows(); i++) {
            ArrayList<GameTile> tileRow = new ArrayList<GameTile>();
            for (int j = 0; j < this.model.getCols(); j++) {
                Vector2 pos = new Vector2(tileSize * j, tileSize * i);
                GameTile tile = new GameTile(pos, sizeVec);
                tile.setSize(tileSize);
                tileRow.add(tile);
            }
            tiles.add(tileRow);
        }
        this.tiles = tiles;
    }

    private void calculateTileSize() {
        int rows = model.getRows();
        tileSize = (int) (width / rows); // rectangular game board
    }

    @Override
    public void update(float dt) {
        super.update(dt);
        updateTiles(dt);
    }

    private void updateTiles(float dt) {
        if (!doneInitializing) return;
        for (int i = 0; i < model.getRows(); i++) {
            for (int j = 0; j < model.getCols(); j++) {
                tiles.get(i).get(j).update(dt);
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (canvas == null) return;
        canvas.drawColor(Color.rgb(34, 139, 34));
        drawTiles(canvas);
    }

    private void drawTiles(Canvas canvas) {
        if (!doneInitializing || canvas == null) return;
        for (int i = 0; i < model.getRows(); i++) {
            for (int j = 0; j < model.getCols(); j++) {
                tiles.get(i).get(j).draw(canvas);
            }
        }
    }

    @Override
    public boolean onTouchDown(MotionEvent touch) {
        int y = (int) touch.getY();
        int x = (int) touch.getX();

        int col = x / tileSize;
        int row = y / tileSize;
        if (model.isInBombMode()) {
            model.setBomb(row, col);
        } else if (model.isInSheepPlacingMode()) {
            model.placeSheep(row, col);
        }
        return true;
    }

}

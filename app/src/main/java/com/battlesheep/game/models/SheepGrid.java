package com.battlesheep.game.models;

import java.util.ArrayList;

import sheep.math.Vector2;

/**
 * Created by Tor Idland on 3/31/14.
 */
public class SheepGrid extends Grid {

    private int sheepCount;

    public SheepGrid(int rows, int cols) {
        super(rows, cols);
    }

    public void setSheep(ArrayList<Vector2> sheep) {
        for (int i = 0; i < sheep.size(); i++) {
            Vector2 pos = sheep.get(i);
            int row = (int) pos.getX();
            int col = (int) pos.getY();
            if (row >= getRows() || col >= getCols()) break;
            set(row, col, TileState.SHEEP);
            sheepCount++;
        }
    }

    public int getSheepCount() {
        return sheepCount;
    }
}

package com.battlesheep.game.models;


public interface OnChangeListener<T> {

    void onChange(T model);

}

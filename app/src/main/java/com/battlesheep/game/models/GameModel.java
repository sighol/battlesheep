package com.battlesheep.game.models;

import com.battlesheep.BLog;
import com.battlesheep.activities.ActivityExtras;
import com.battlesheep.network.GetGameCommand;
import com.battlesheep.network.NetworkCommand;
import com.battlesheep.network.NewMoveCommand;
import com.battlesheep.network.PlaceSheepCommand;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import sheep.math.Vector2;

public class GameModel extends SimpleObservable<GameModel> {

    private int id;

    private int userId;
    private String username;

    private int opponentId;
    private String opponentUsername;

    private int rows = 8;
    private int cols = 8;

    private ArrayList<Vector2> userSheepPositions = new ArrayList<>();
    private ArrayList<Vector2> opponentSheepPositions = new ArrayList<>();

    private ArrayList<Vector2> userBombPositions = new ArrayList<>();
    private ArrayList<Vector2> opponentBombPositions = new ArrayList<>();

    public GameModel(int id, int userId) {
        this.id = id;
        this.userId = userId;
    }

    public void load() {
        Map<String, String> args = new HashMap<String, String>();
        args.put(ActivityExtras.gameId, "" + id);
        args.put(ActivityExtras.userId, "" + userId);

        GetGameCommand cmd = new GetGameCommand(args) {
            @Override
            public void onFinished(JSONArray json) throws JSONException {
                clearArrays();

                JSONObject obj = json.getJSONObject(0);
                setUserinfo(obj);
                setBombinfo(obj);
                setSheepPositioninfo(obj);

                notifyObservers(GameModel.this);
            }

            private void clearArrays() {
                opponentBombPositions.clear();
                userBombPositions.clear();
                userSheepPositions.clear();
                opponentSheepPositions.clear();
            }

            private void setSheepPositioninfo(JSONObject obj) throws JSONException {
                JSONArray user1sheep = obj.getJSONArray("user1_pos");
                JSONArray user2sheep = obj.getJSONArray("user2_pos");
                for (int i = 0; i < user1sheep.length(); i++) {
                    int x = user1sheep.getJSONArray(i).getInt(0);
                    int y = user1sheep.getJSONArray(i).getInt(1);
                    userSheepPositions.add(i, new Vector2(x, y));
                }
                for (int i = 0; i < user2sheep.length(); i++) {
                    int x = user2sheep.getJSONArray(i).getInt(0);
                    int y = user2sheep.getJSONArray(i).getInt(1);
                    opponentSheepPositions.add(i, new Vector2(x, y));
                }
            }

            private void setBombinfo(JSONObject obj) throws JSONException {
                JSONArray user1moves = obj.getJSONArray("user1_moves");
                JSONArray user2moves = obj.getJSONArray("user2_moves");

                for (int i = 0; i < user1moves.length(); i++) {
                    int x = user1moves.getJSONArray(i).getInt(0);
                    int y = user1moves.getJSONArray(i).getInt(1);
                    userBombPositions.add(i, new Vector2(x, y));
                }
                for (int i = 0; i < user2moves.length(); i++) {
                    int x = user2moves.getJSONArray(i).getInt(0);
                    int y = user2moves.getJSONArray(i).getInt(1);
                    opponentBombPositions.add(i, new Vector2(x, y));
                }
            }

            private void setUserinfo(JSONObject obj) throws JSONException {
                JSONObject user = obj.getJSONObject("user1");
                username = user.getString("username");

                JSONObject opponent = obj.getJSONObject("user2");
                opponentUsername = opponent.getString("username");
                opponentId = opponent.getInt("id");
            }
        };
       cmd.execute();
    }

    public void sendSheepPosition(ArrayList<Vector2> sheepPositions) {
        BLog.v(this, "set sheepPosition");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sheepPositions.size(); i++) {
            if (i > 0) {
                sb.append("-");
            }
            Vector2 pos = sheepPositions.get(i);
            String s = "" + pos.getX() + "," + pos.getY();
            sb.append(s);
        }
        Map<String, String> args = new HashMap<String,String>();
        args.put(ActivityExtras.userId, "" + userId);
        args.put(ActivityExtras.gameId, "" + id);
        args.put("pos", sb.toString());
        NetworkCommand cmd = new PlaceSheepCommand(args) {
            @Override
            public void onFinished(JSONArray json) throws JSONException {
                load();
            }
        };
        cmd.execute();
    }

    public boolean isInBombMode() {
        boolean isInBombMode = userSheepPositions.size() > 0;
        return isInBombMode;
    }

    public boolean isInSheepPlacingMode() {
        return userSheepPositions.size() == 0;
    }

    public boolean isMyTurn() {
        BLog.v(this, "uspos: %d, oppos: %d, oppbobs: %d, ubobs: %d",
                userSheepPositions.size(), opponentSheepPositions.size(),
                opponentBombPositions.size(), userBombPositions.size());

        return userSheepPositions.size() > 0 && opponentSheepPositions.size() > 0 &&
                opponentBombPositions.size() >= userBombPositions.size();
    }

    public void setBomb(int row, int col) {
        Map<String, String> args = new HashMap<>();
        args.put(ActivityExtras.userId, "" + userId);
        args.put(ActivityExtras.gameId, "" + id);

        String pos = "" + row + "," + col;
        args.put("pos", pos);
        NetworkCommand cmd = new NewMoveCommand(args) {
            @Override
            public void onFinished(JSONArray json) throws JSONException {
                load();
            }
        };
        cmd.execute();
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public int getOpponentId() {
        return opponentId;
    }

    public String getOpponentUsername() {
        return opponentUsername;
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public ArrayList<Vector2> getUserSheepPositions() {
        return userSheepPositions;
    }

    public ArrayList<Vector2> getOpponentSheepPositions() {
        return opponentSheepPositions;
    }

    public ArrayList<Vector2> getUserBombPositions() {
        return userBombPositions;
    }

    public ArrayList<Vector2> getOpponentBombPositions() {
        return opponentBombPositions;
    }
}

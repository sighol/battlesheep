package com.battlesheep.game.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.ImageView;
import android.view.View;

import com.battlesheep.R;
import sheep.game.Sprite;
import sheep.graphics.Image;
import sheep.graphics.SpriteView;
import sheep.math.Vector2;

/**
 * Created by Silje on 12.03.14.
 */
public class GameTile extends Sprite {
    private static Image backgroundImage = new Image(R.drawable.gress);
    private static Image sheepImage = new Image(R.drawable.sheep);
    private static Image missedImage = new Image(R.drawable.miss);
    private static Image deadSheep = new Image(R.drawable.deadsheep);
    private TileState state = TileState.EMPTY;

    private Vector2 size;
    private Image currentImage = backgroundImage;

    public GameTile(Vector2 pos, Vector2 size) {
        super(backgroundImage);
        setPosition(pos);
        this.size = size;
    }

    @Override
    public void draw(Canvas canvas) {
        if (canvas == null) return;
        setSize(size.getX());
        switch (state) {
            case EMPTY:
                setImage(backgroundImage);
                break;
            case SHEEP:
                setImage(sheepImage);
                break;
            case MISSED:
                setImage(missedImage);
                break;
            case HIT:
                setImage(deadSheep);

                break;
        }

        super.draw(canvas);
    }

    @Override
    public void update(float dt) {
        super.update(dt);
    }

    public void setSize(float size) {
        float w = currentImage.getWidth();
        float scale = size / w * 0.9f;
        super.setOffset(0, 0);
        super.setScale(scale, scale);
    }

    private void setImage(Image image) {
        setView(image);
        currentImage = image;
    }

    public void setTileState(TileState state) {
        this.state = state;
    }
}

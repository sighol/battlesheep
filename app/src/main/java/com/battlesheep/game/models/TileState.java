package com.battlesheep.game.models;

/**
 * Created by oda on 22.03.14.
 */
public enum TileState {
    EMPTY(0),
    SHEEP(1),
    MISSED(2),
    HIT(3);

    private final int value;

    private TileState(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

package com.battlesheep.game.models;

import java.util.ArrayList;

import sheep.math.Vector2;

/**
 * Created by Tor Idland on 3/31/14.
 */
public class BombGrid extends Grid {

    private int hitCount;
    private int missCount;

    private SheepGrid sheepGrid;

    public BombGrid(int rows, int cols) {
        super(rows, cols);
    }

    public void setBombsAndSheep(ArrayList<Vector2> bombs, ArrayList<Vector2> sheep) {

        sheepGrid = new SheepGrid(rows, cols);
        sheepGrid.setSheep(sheep);

        for (int i = 0; i < bombs.size(); i++) {
            Vector2 pos = bombs.get(i);
            int row = (int) pos.getX();
            int col = (int) pos.getY();
            if (row >= getRows() || col >= getCols()) break;
            if (sheepGrid.get(row, col) == TileState.SHEEP) {
                set(row, col, TileState.HIT);
                hitCount++;
            } else {
                set(row, col, TileState.MISSED);
                missCount++;
            }
        }
    }

    public int getHitCount() {
        return hitCount;
    }

    public double getCompletionRate() {
        return (double) getHitCount() / sheepGrid.getSheepCount();
    }

    public int getMissCount() {
        return missCount;
    }

    public SheepGrid getSheepGrid() {
        return sheepGrid;
    }
}

package com.battlesheep.game.models;

import com.battlesheep.BLog;
import java.util.ArrayList;
import sheep.math.Vector2;

public class BoardModel extends SimpleObservable<BoardModel> implements OnChangeListener<GameModel> {

    private SheepGrid sheepGrid;
    private BombGrid opponentBombGrid;
    private BombGrid bombGrid;

    private Grid currentGrid;

    private GameModel gameModel;
    private int sheepPositionCount;
    public static final int MAX_SHEEP_COUNT = 10;
    private boolean hasPlacedBombAndIsWaitingOnUpdate = false;
    private boolean isLoading;

    private boolean hasWon = false;
    private boolean hasLost = false;

    public BoardModel(GameModel gameModel) {
        this.gameModel = gameModel;
        gameModel.addListener(this);
        initGrids(gameModel);
    }

    @Override
    public void onChange(GameModel model) {
        initGrids(model);
        if (isInBombMode()) {
            if (bombGrid.getCompletionRate() == 1.0) {
                hasWon = true;
            } else if (opponentBombGrid.getCompletionRate() == 1.0) {
                hasLost = true;
            }
        }
        hasPlacedBombAndIsWaitingOnUpdate = false;
        isLoading = false;
        notifyObservers(this);
    }

    private void initGrids(GameModel model) {
        bombGrid = new BombGrid(model.getRows(), model.getCols());
        bombGrid.setBombsAndSheep(model.getUserBombPositions(), model.getOpponentSheepPositions());

        opponentBombGrid = new BombGrid(model.getRows(), model.getCols());
        opponentBombGrid.setBombsAndSheep(model.getOpponentBombPositions(), model.getUserSheepPositions());

        sheepGrid = opponentBombGrid.getSheepGrid();
        sheepPositionCount = sheepGrid.getSheepCount();

        if (isInBombMode()) {
            currentGrid = bombGrid;
        } else {
            currentGrid = sheepGrid;
        }
    }

    public int getRows() {
        return this.gameModel.getRows();
    }

    public int getCols() {
        return this.gameModel.getCols();
    }

    public TileState get(int row, int col) {
        return currentGrid.get(row, col);
    }

    public void set(int row, int col, TileState state) {
        currentGrid.set(row, col, state);
    }

    public void sendSheepPositions() {
        ArrayList<Vector2> positions = new ArrayList<Vector2>();
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                if (get(i, j) == TileState.SHEEP) {
                    positions.add(new Vector2(i, j));
                }
            }
        }
        if (positions.size() < MAX_SHEEP_COUNT) {

        } else {
            gameModel.sendSheepPosition(positions);
        }
    }

    public boolean isInBombMode() {
        return gameModel.isInBombMode();
    }

    public boolean isInSheepPlacingMode() {
        return gameModel.isInSheepPlacingMode();
    }

    public void setBomb(int row, int col) {
        if (isMyTurn()) {
            BLog.v(this, "placing bombs YEAH");
            gameModel.setBomb(row, col);
            hasPlacedBombAndIsWaitingOnUpdate = true;
        } else {
            BLog.v(this, "It's not my turn yet. Please be seated and wait for the signal");
        }
    }

    public void placeSheep(int row, int col) {
        if (!isInSheepPlacingMode()) {
            throw new RuntimeException("Not in sheep placing mode");
        }
        if (get(row, col) == TileState.EMPTY && sheepPositionCount + 1 <= MAX_SHEEP_COUNT) {
            set(row, col, TileState.SHEEP);
            sheepPositionCount++;
        } else if (get(row, col) == TileState.SHEEP) {
            set(row, col, TileState.EMPTY);
            sheepPositionCount--;
        }
        notifyObservers(this);
    }

    public double getCompletionRate(){
        if (isInSheepPlacingMode()) {
            return 0;
        } else if (isInBombMode()) {
            double rate = bombGrid.getCompletionRate();
            BLog.v(this, "comprate: " + rate);
            return rate;
        } else {
            return 0;
        }
    }

    public double getOpponentCompletionRate() {
        if (isInSheepPlacingMode()) {
            return 0;
        } else if (isInBombMode()) {
            double rate = opponentBombGrid.getCompletionRate();
            BLog.v(this, "opponent comp rate: " + rate);
            return rate;
        }
        return 0;
    }

    public boolean isMyTurn() {
        return gameModel.isMyTurn() && !hasPlacedBombAndIsWaitingOnUpdate;
    }

    public String getMode() {
        StringBuilder sb = new StringBuilder();
        if (isInBombMode()) {
            sb.append("BombMode ");
            if (isMyTurn()) {
                sb.append("my-turn ");
            } else {
                sb.append("not-my-turn");
            }
        }
        if (isInSheepPlacingMode()) {
            sb.append("Sheep-placing-mode ");
        }
        if (isLoading) {
            sb.append("Is Loading ");
        }
        return sb.toString();
    }

    public void refresh() {
        isLoading = true;
        notifyObservers(this);
        gameModel.load();
    }

    public boolean hasWon() {
        return hasWon;
    }

    public boolean hasLost() {
        return hasLost;
    }
}

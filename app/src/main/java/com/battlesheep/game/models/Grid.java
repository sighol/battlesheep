package com.battlesheep.game.models;

import java.util.ArrayList;

import sheep.math.Vector2;


public class Grid {

    private ArrayList<ArrayList<TileState>> grid;

    protected final int rows;
    protected final int cols;


    public Grid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        initGrid();
    }

    private void initGrid() {
        ArrayList<ArrayList<TileState>> grid = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            ArrayList<TileState> row = new ArrayList<>();
            for (int j = 0; j < cols; j++) {
                row.add(TileState.EMPTY);
            }
            grid.add(row);
        }
        this.grid = grid;
    }

    public TileState get(int row, int col) {
        if (row >= getRows() || col >= getCols()) return TileState.EMPTY;
        return grid.get(row).get(col);
    }

    public void set(int row, int col, TileState state) {
        if (row >= getRows() || col >= getCols()) return;
        grid.get(row).set(col, state);
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }
}
